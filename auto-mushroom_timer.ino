long hours_1_millis = 3600000; //number of millis in an hour
long hours_12_millis = 43200000; //number of millis in 12 hours
long hours_8_millis = 28800000; //number of millis in 8 hours
long hours_16_millis = 57600000; //number of millis in 16 hours
long preWaterTime = 0;
long lightOnTime = 0;
long lightOffTime = 0;
long currTime = 0;
bool lightOn = false;
void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:
  currTime = millis();
  if( (currTime - preWaterTime) >= hours_12_millis)
  {
    preWaterTime = currTime;
    //water mushroom
  }
  if(lightOn)
  {
    if( (currTime-lightOnTime) >= hours_8_millis)
    {
      lightOffTime = currTime;
      lightOn = false;
      //turn off light
    }
  }
  else
  {
    if( (currTime-lightOffTime) >= hours_16_millis)
    {
      lightOnTime = currTime;
      lightOn = true;
      //turn light on
    }
  }
  

}
